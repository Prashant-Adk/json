// temp json data
let defaultData = {
    "squadName": "Super hero squad",
    "homeTown": "Metro City",
    "formed": 2016,
    "secretBase": "Super tower",
    "active": true,
    "members": [
      {
        "name": "Molecule Man",
        "age": 29,
        "secretIdentity": "Dan Jukes",
        "powers": [
          "Radiation resistance",
          "Turning tiny",
          "Radiation blast"
        ]
      },
      {
        "name": "Madame Uppercut",
        "age": 39,
        "secretIdentity": "Jane Wilson",
        "powers": [
          "Million tonne punch",
          "Damage resistance",
          "Superhuman reflexes"
        ]
      },
      {
        "name": "Eternal Flame",
        "age": 1000000,
        "secretIdentity": "Unknown",
        "powers": [
          "Immortality",
          "Heat Immunity",
          "Inferno",
          "Teleportation",
          "Interdimensional travel"
        ]
      }
    ]
}

// testing sth with these data
let StudentData = [
    {"name":"prashant", "address":"amarsing", "clz":"Pokhara University"},
    {"name":"prashant", "address":"kathmandu", "clz":"Tribhuvan University"},
    {"name":"prashant", "address":"lalitpur", "clz":"kathmandu University"}
]

let teacherData = [
    {"name":"shiva", "address":"lamachaur", "clz":"PCM"},
    {"name":"rahul", "address":"bijaypur", "clz":"WRC"}
]

let stdata = {
    "name": "prashant",
    "score": "4.5",
    "isOnline": true,
    "isAdmin": false,
    "Comment": null,
    "shopItems": ["food", "clothes", "momo", "jacket"],
    "newObj": [
        {
            "name": "hero",
            "score": "4.3",
            "isOnline": false,
            "shopItems": ["food", "clothes", "momo", { "a": true }]
        },
        {
            "name": "zero",
            "score": "3.9",
            "isOnline": true,
            "shopItems": ["food", "jacket", "momo", { "a": "hihello" }]
        }
    ]
}

module.exports = {
    defaultData,
    StudentData,
    teacherData,
    stdata
}